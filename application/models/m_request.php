<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_request extends CI_Model
{
    public function get()
    {
        $hasil = $this->db->get('request');
        if ($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return array();
        }
    }

    public function find($request_id)
    {
        $hasil = $this->db->where('request_id', $request_id)
            ->limit(1)
            ->get('request');
        if ($hasil->num_rows() > 0) {
            return $hasil->row();
        } else {
            return array();
        }
    }
    public function update($request_id, $data_requests)
    {
        $this->db->where('request_id',$request_id)
            ->update('request', $data_requests);
        }
    public function delete($request_id)
    {
        $this->db->where('request_id', $request_id)->delete('request');
    }
}
