<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_pasien extends CI_Model
{
    public function get()
    {
        $hasil = $this->db->get('pasien');
        if ($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return array();
        }
    }
    public function getHistory($id)
    {
        $hasil = $this->db->select('*')->from('user')->join('request', 'user.pasien_id = request.pasien_id')->where('user.username', $id)->order_by('request.request_id', 'desc')->get()->result();
        return $hasil;
    }
    

    public function find($pasien_id)
    {
        $hasil = $this->db->where('pasien_id', $pasien_id)
            ->limit(1)
            ->get('user');
        if ($hasil->num_rows() > 0) {
            return $hasil->row();
        } else {
            return array();
        }
    }
    public function findUsername($username)
    {
        $hasil = $this->db->where('username', $username)
            ->limit(1)
            ->get('pasien');
        if ($hasil->num_rows() > 0) {
            return $hasil->row();
        } else {
            return array();
        }
    }
    public function update($pasien_id, $data_users)
    {
        $this->db->where('pasien_id',$pasien_id)
            ->update('pasien', $data_pasiens);
        }
    public function delete($pasien_id)
    {
        $this->db->where('pasien_id', $pasien_id)->delete('pasien');
    }
}
