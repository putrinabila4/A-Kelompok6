<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_konsultan extends CI_Model
{
    public function get()
    {
        $hasil = $this->db->get('konsultan');
        if ($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return array();
        }
    }

    public function find($konsultan_id)
    {
        $hasil = $this->db->select('*')->from('user')->join('konsultan','user.username = konsultan.username')->where('user.username', $konsultan_id)
            ->limit(1)
            ->get();
        if ($hasil->num_rows() > 0) {
            return $hasil->row();
        } else {
            return array();
        }
    }
    public function findID($konsultan_id)
    {
        $hasil = $this->db->select('*')->from('user')->join('konsultan','user.username = konsultan.username')->where('user.konsultan_id', $konsultan_id)
            ->limit(1)
            ->get();
        if ($hasil->num_rows() > 0) {
            return $hasil->row();
        } else {
            return array();
        }
    }
    public function findRequest($id){
        $hasil = $this->db->where('request_id',$id)->get('request')->row();
        return $hasil;
    }
    public function getPasien($id)
    {
        $hasil = $this->db->select('*')->from('konsultan')->join('request', 'konsultan.konsultan_id = request.konsultan_id')->join('user','user.pasien_id = request.pasien_id')->where('konsultan.username', $id)->order_by('request.request_id', 'desc')->get()->result();
        return $hasil;
    }
    public function findUsername($konsultan_id)
    {
        $hasil = $this->db->where('username', $konsultan_id)
            ->limit(1)->get('konsultan');
        if ($hasil->num_rows() > 0) {
            return $hasil->row();
        } else {
            return array();
        }
    }
    public function update($konsultan_id, $data_konsultans)
    {
        $this->db->where('username',$konsultan_id)
            ->update('konsultan', $data_konsultans);
        }
        public function updateRequest($id, $data){
            $this->db->where('request_id',$id)
                ->update('request', $data);
            }    
    public function delete($konsultan_id)
    {
        $this->db->where('konsultan_id', $konsultan_id)->delete('konsultan');
    }
}
