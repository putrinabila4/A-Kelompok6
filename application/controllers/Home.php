
<?php

class Home extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_pasien');
        $this->load->model('m_konsultan');
    }

    public function index(){
        $this->load->view('header');
        $this->load->view('beranda');
    }
    public function Login(){
        $this->load->view('header');
        $this->load->view('Login');
    }

    public function Pasien(){
        
        $this->load->view('header');
        $username = $this->session->userdata('user_name');
        $data['konsultan'] =$this->m_user->getKonsultan();
        $data['profile'] = $this->m_user->find($username);
        $this->load->view('Pasien',$data);
    }
    public function Konsultan(){
        
        $this->load->view('header');
        $username = $this->session->userdata('user_name');
        var_dump($username);
        $data['pasien'] = $this->m_konsultan->getPasien($username);
        $data['profile'] = $this->m_konsultan->find($username);
        $this->load->view('Konsultan',$data);
    }
    

    public function RegisterPasien(){
        
        $this->load->view('header');
        $this->load->view('Register');
    }
    public function doLogin(){
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_message('required', 'Enter %s');
            if ($this->form_validation->run() == false) {
                redirect('Home/Login');
            } else {
                $username    = $this->input->post('username');
                $password = $this->input->post('password');
                $user     = $this->db->get_where('user', ['username' => $username])->row_array();
                var_dump($username);
                var_dump($user);
                if ($user) {
                    if (password_verify($password, $user['password'])) {
                        $currentUser = $this->m_user->find($username);
                        $username = $currentUser->username;
                        $this->session->set_userdata('user_name', $username);
                        if ($currentUser->pasien_id != 0 && $currentUser->konsultan_id == NULL ){
                        $role = $currentUser->pasien_id;
                        redirect('Home/Pasien');
                        }else 
                            $role = $currentUser->konsultan_id;
                            redirect('Home/Konsultan');
                       
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert">
                Wrong Password!
              </div>');
                        redirect('Home/Login');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert">
                Email is not registered!
              </div>');                }
            }

    }
    public function doRegister(){
        $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('nohp', 'Nomor HP', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
        $this->form_validation->set_message('required', 'Enter %s');
        if ($this->form_validation->run() == false){
            redirect('Home');
        } else{
            if ($this->input->post('level') == '1'){
            $data_pasien = [
                'favorite' => NULL,
                'category' => NULL,
                'username' => $this->input->post('username')
            ];
            $this->db->insert('pasien',$data_pasien);
            $pasienID = $this->m_pasien->findUsername($this->input->post('username'));
            $data = [
                'alamat' => $this->input->post('alamat'),
                'username' => $this->input->post('username'),
                'nama' => $this->input->post('fullname'),
                'email' => $this->input->post('email'),
                'hp' => $this->input->post('nohp'),
                'photo' => 'defaultProfile.jpg',
                'pasien_id' => $pasienID->pasien_id,
                'password' =>password_hash($this->input->post('password'),PASSWORD_DEFAULT), 
            ];
            $this->db->insert('user', $data); 
            redirect('Home/Login');
        }
           else if ($this->input->post('level') == 2){
            $data_konsultan = [
                'avail' => '1',
                'category' => NULL,
                'workhour' => NULL,
                'username' => $this->input->post('username')
            ];
            $this->db->insert('konsultan',$data_konsultan);
            $konsultanID = $this->m_konsultan->findUsername($this->input->post('username'));
            $data = [
                'alamat' => $this->input->post('alamat'),
                'username' => $this->input->post('username'),
                'nama' => $this->input->post('fullname'),
                'email' => $this->input->post('email'),
                'hp' => $this->input->post('nohp'),
                'pasien_id' => NULL,
                'photo' => 'defaultProfile.jpg',
                'konsultan_id' => $konsultanID->konsultan_id,
                'password' =>password_hash($this->input->post('password'),PASSWORD_DEFAULT), 
            ];
            $this->db->insert('user', $data); 
            redirect('Home/Login');           
           }
    }
}

    public function AboutUs(){
        $this->load->view('header');
        $this->load->view('AboutUs');
    }
    public function addKonsultasi($id){
        $this->form_validation->set_rules('complaint', 'Complain', 'required');     
        if ($this->form_validation->run() == false){
            redirect('Home');
        }else {
            $konsultanID = $this->m_konsultan->findID($id);
            $pasienID = $this->m_user->find($this->session->userdata('user_name'));
            $data = array (
                'date' => date('Y-m-d H:i:s'),
                'complaint' =>$this->input->post('complaint'),
                'pasien_id' =>$pasienID->pasien_id,
                'konsultan_id' =>$konsultanID->konsultan_id
            );
            $this->db->insert('request',$data); 
            redirect('Home/Pasien');
        }
    }
    public function updateKonsultasi($id){
        $this->form_validation->set_rules('suggestion', 'Suggestion', 'required');     
        if ($this->form_validation->run() == false){
            redirect('Home');
        }else {
            $konsultanID = $this->m_konsultan->findRequest($id);
            $pasienID = $this->m_user->find($this->session->userdata('user_name'));
            $data = array (
                'suggestion' =>$this->input->post('suggestion'),
                );
            $this->m_konsultan->updateRequest($id,$data);
            redirect('Home/Konsultan');
        }
    }
    public function changeProfile(){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_error_delimiters('<div style="color:red">', '</div>');
        $upload['upload_path'] = './assets/';
        $upload['allowed_types'] = 'jpg|png';
        $username = $this->session->userdata('user_name');
        $upload['file_name'] = $username;
        $this->load->library('upload', $upload);
        if ($this->upload->do_upload('uploadPhoto')) {
            $image = $this->upload->data();
            $data_user = [
                'nama' => $this->input->post('name'),
                'alamat' => $this->input->post('address'),
                'email' => $this->input->post('email'),
                'photo' => $image['file_name']
                ];
                $this->m_user->updateUser($username, $data_user);
                redirect('Home/Pasien');
        }   
    }
    public function changeProfileKonsultan(){
        $upload['upload_path'] = './assets/';
        $upload['allowed_types'] = 'jpg|png';
        $username = $this->session->userdata('user_name');
        $upload['file_name'] = $username;
        $this->load->library('upload', $upload);
        if ($this->upload->do_upload('uploadPhoto')) {
            $image = $this->upload->data();
            $data_user = [
                'alamat' => $this->input->post('address'),
                'email' => $this->input->post('email'),
                'photo' => $image['file_name']
                ];
                $this->m_user->updateUser($username, $data_user);
            $data_konsultan = [
                'workhour' => $this->input->post('Workhour'),
                'category' => $this->input->post('category')
            ];
                $this->m_konsultan->update($username,$data_konsultan);
                redirect('Home/Konsultan');
        }   
    }
    public function viewHistory($id){
        $data['pasien'] = $this->m_pasien->getHistory($id);
        $this->load->view('header');
        $data['profile'] = $this->m_user->find($id);
        $this->load->view('PasienHistory',$data);   
        
    }
    public function tolak($id){
        $this->db->where('request_id',$id)->delete('request');
        redirect ('Home/Konsultan');
    }
    public function updateService($id,$id2){
        if ($id2 == 1) {$data = [
            'avail' => 0
        ];
        $this->m_konsultan->update($id,$data);
        redirect('Home/Konsultan');
    } else if ($id2 == 0){ 
        $data = [
            'avail' => 1
        ];
        $this->m_konsultan->update($id,$data);
        redirect('Home/Konsultan'); 
    }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect('Home');
    }
}