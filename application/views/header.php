<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fa/css/fontawesome.min.css">

    <title>MENTAL HEALTH CONSULTANT</title>
  </head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="<?=base_url()?>Home">MENTAL HEALTH CONSULTANT</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarColor01">
          <ul class="navbar-nav mr-auto">
        
          </ul>
          <ul class="form-inline navbar-nav">
                <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>Home">Home</a>
                     </li>
                <?php if(!$this->session->userdata('user_name')) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('home/RegisterPasien');?>">Register</a>
                 </li>
                <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('home/Login');?>">Login</a>
                </li>
                <?php endif;?>
                <?php if ($this->session->userdata('user_name')) : ?>
                  <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('home/logout');?>">Logout</a>
                </li>
                <?php endif;?>
                <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('home/AboutUs');?>">About Us</a>
                </li>
</nav>
</body>          
 </html>   