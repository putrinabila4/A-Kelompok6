<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fa/css/fontawesome.min.css" />

    <title>Register</title>
  </head>
  <body>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
                <div class="card border-primary" >
                        <div class="card-header text-center text-primary"><h4>Register</h4></div>
                            <div class="card-body">
                            <form class= "register" action="<?php echo base_url('Home/doRegister')?>" method="post">
                                <div class="form-group">
                                    <label >Username</label>
                                                                        <input type="text" name="username" class="form-control"  placeholder="Masukkan Username">
                                 </div>
                                 <div class="form-group">
                                        <label >Nama Lengkap</label>
                                        <input type="text" name = "fullname" class="form-control"  placeholder="Masukkan Nama Lengkap">
                                     </div>
                               <div class="form-group">
                                    <label >Alamat</label>
                                    <input type="text" class="form-control"  name ="alamat" placeholder="Masukkan Alamat">
                                 </div>
                                 <div class="form-group">
                                        <label >Email</label>
                                        <input type="email" class="form-control" name="email" placeholder="Masukkan Email">
                                     </div>

                                  <div class="form-group">
                                        <label >Nomor Hp</label>
                                        <input type="text" class="form-control" name="nohp" placeholder="Masukkan Nomor HP">
                                     </div>
                                     <div class="form-group">
                                        <label >Jenis Akun</label>
                                        <input type="radio" name="level" placeholder="Masukkan Nomor HP" value="1"> Pasien
                                        <input type="radio" name="level" placeholder="Masukkan Nomor HP" value="2"> Konsultan
                                    </div>
                                 
                                <div class="form-group">
                                    <label >Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="Masukkan Password">
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
                                </div>
                            </form>
                        </div>
                </div>
              
        <div class="col-3"></div>
    </div>

</div>
<div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <footer class=" card text-white bg-primary">
        
             <br>
                <p class="text-center">Copyright @ 2019 - MENTAL HEALTH CONSULTANT</p>
            <br>
            </footer>
        </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js" ></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
  </body>
</html> 