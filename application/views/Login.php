<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fa/css/fontawesome.min.css" />
    
    <title>Login </title>
  </head>
  <body>
    <br><br><br><br>
      <div class="container">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4">
                <div class="card border-primary mb-3" style="max-width: 20rem;">
                <form class= "register" action="<?php echo base_url('Home/doLogin')?>" method="post">
                <div class="card-header text-center text-primary"><h4>Login</h4></div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label >Username</label>
                                    <input type="text" name="username" class="form-control"  placeholder="Masukan Username">
                                 </div>
                                <div class="form-group">
                                    <label >Password</label>
                                    <input type="password" name="password" class="form-control"  placeholder="Masukkan Password">
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
                        </div>  
                        </div>
            </form>
                    </div>
              
        <div class="col-4"></div>
    </div>

</div>

<div>

        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <footer class=" card text-white bg-primary">
        
             <br>
                <p class="text-center">Copyright @ 2019 - MENTAL HEALTH CONSULTANT</p>
            <br>
            </footer>
        </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js" ></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
  </body>
</html>