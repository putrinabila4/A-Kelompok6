<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fa/css/fontawesome.min.css" />
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- jQuery Modal -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
 
  <style>
    /* Center the image and position the close button */

    .imgcontainer {
      text-align: center;
      margin: 24px 0 12px 0;
      position: relative;
    }

    .avatar {
      width: 50%;
      border-radius: 50%;
      margin-left: auto;
      margin-right: auto;
    }

    /* modal */
    .imgcontainer {
      text-align: center;
      margin: 24px 0 12px 0;
      position: relative;
    }

    .img {
      width: 50%;
      border-radius: 50%;
      margin-left: auto;
      margin-right: auto;
    }

    #name,
    #user,
    #pass,
    #address,
    #email,
    #nohp,
    #label {
      color: black;
    }

    /* The Modal (background) */
    .modal {
      display: none;
      position: fixed;
      z-index: 1;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      overflow: auto;
      background-color: rgba(0, 0, 0, 0.4);
    }

    /* Modal Content Box */
    .modal-content {
      background-color: #fefefe;
      margin: 4% auto 15% auto;
      border: 1px solid #888;
      width: 40%;
      padding-bottom: 30px;
    }

    /* The Close Button (x) */
    .close {
      position: absolute;
      right: 25px;
      top: 0;
      color: #000;
      font-size: 35px;
      font-weight: bold;
    }

    .close:hover,
    .close:focus {
      color: red;
      cursor: pointer;
    }

    #save {
      border-radius: 1.5em;
      background-color: #4caf50;
      color: white;
      padding: 10px 5px;
      border: none;
      cursor: pointer;
      width: 40%;
      font-size: 20px;
    }

    button:hover {
      opacity: 0.8;
    }

    input[type="text"] {
      width: 90%;
      padding: 12px 20px;
      margin: 8px 26px;
      display: inline-block;
      border: 1px solid #ccc;
      box-sizing: border-box;
      font-size: 16px;
    }
  </style>
  <title>Login Pasien</title>
</head>

<body>
  <br>
  <br>
  <br>


  <div class="container">
    <div class="row">
      <div class="col-8">
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">No.</th>
              <th scope="col">Name of Pasien</th>
              <th scope="col">Tanggal</th>
              <th scope="col">Komplain</th>
              <th scope="col">Suggestion</th>
              </tr>
          </thead>
          <tbody>
            <?php foreach ($pasien as $kons) : ?>
              <tr class="table-active">
                <td> <?= $kons->request_id?> </td>
                <td> <?= $kons->nama?></td>
                <td> <?= $kons->date?></td>
                <td> <?= $kons->complaint?></td>
                <td> <?= $kons->suggestion?></td>
                </tr>
              <div id="<?=$kons->request_id?>" class="modal" style="margin-left : 400px">

<div class="imgcontainer">
  <img src="<?= base_url() ?>/assets/img/doctor.jpg" alt="Avatar" class="avatar">
</div>
<?= form_open_multipart('Home/updateKonsultasi/'.$kons->request_id, ['class'=>'form-horizontal']) ?>      
<div class="container-fluid" style="text-align: center">
  <input id="suggestion" type="text" placeholder="Enter Your Complaint" name="suggestion" required>

  <button id="save" type="submit">Save</button>
</div>

            <?php endforeach ?>
          </tbody>
        </table>
      </div>
      <div class="col-4">

        <div class="card mb-3">
          <h3 class="card-header">Profile Pasien</h3>
          <div class="card-body">
          </div>
          <img style="height: 100%; width: 100%; display: block;" src="<?php echo base_url('assets/') . $profile->photo; ?>">
          <div class="card-body">
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item"><?= $profile->nama?></li>
            <li class="list-group-item"><?= $profile->alamat?> </li>
            <li class="list-group-item"><?= $profile->email?></li>
            <li class="list-group-item"><?= $profile->hp ?></li>
            <a class="btn btn-primary" onclick="document.getElementById('modal-edit').style.display='block'"> Edit Profile </a>
          </ul>
        </div>
      </div>
    </div>

  </div>

  <div>

    <br>
    <br>
    <footer class=" card text-white bg-primary">

      <br>
      <p class="text-center">Copyright @ 2019 - MENTAL HEALTH CONSULTANT</p>
      <br>
    </footer>

    
    <div id="modal-edit" class="modal" style="margin-left : 400px">
        <div class="imgcontainer">
          <span onclick="document.getElementById('modal-edit').style.display='none'" class="close" title="Close PopUp">&times;</span>
          <img src="<?= base_url() ?>/assets/img/edit.png" alt="edit" class="img" />
        </div>

        <div class="container-fluid" style="text-align: center">
        <?= form_open_multipart('Home/changeProfile');?>
        <input id="name" type="text" placeholder="Edit Your Name" name="name" />
          <input id="address" type="text" placeholder="Edit Your Address" name="address" />
          <input id="email" type="text" placeholder="Edit Your Email" name="email" />
          <input id="nohp" type="text" placeholder="Edit Your Number Handphone" name="nohp" />

          <label id="label">Choose your photo</label>
          <br />
          <input type="file" name="uploadPhoto" style="margin-left:150px; margin-bottom:50px" />

          <button id="save" type="submit">Save</button>
           <?= form_close();?>
        </div>
      </div>
    </div>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>

</html>